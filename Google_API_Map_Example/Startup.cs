﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Google_API_Map_Example.Startup))]
namespace Google_API_Map_Example
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
