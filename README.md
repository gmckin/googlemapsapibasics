# README #

This is a Visual Studio generated Web Application with no database.

### What is this repository for? ###

* Basic Google Map API Example
* Version 1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Clone the repository locally or download the zip.
* Open in Visual Studio Enterprise Edition.
* No database

### Who do I talk to? ###

* Repo owner and admin: Greg McKinney